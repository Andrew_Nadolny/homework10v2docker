import { fightersDetails, fighters } from './mockData';
import { Fighter } from '../fighter';


const API_URL : string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI : boolean = false;

async function callApi(endpoint : string, method : string) : Promise<Fighter[]> {
  const url : string = API_URL + endpoint;
  const options  = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result)=> JSON.parse(atob(result.content)))
        .catch((error : Error) => {
          throw error;
        });

}

async function fakeCallApi(endpoint : string) : Promise<Fighter[]> {
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response as Fighter[]) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint : string) {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === id) as Fighter;
}

export { callApi };
export { getFighterById };

