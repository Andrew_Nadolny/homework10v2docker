

export function createElement({ tagName, className ="", attributes = {}} : { tagName : string, className? : string, attributes? : {[key: string]: string;} }  ) : HTMLElement {
  const element : HTMLElement = document.createElement(tagName);
  
  if (className) {
    element.classList.add(className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
} 