
FROM node:latest as build


WORKDIR /usr/local/app

COPY ./ /usr/local/app/

RUN npm install
RUN npx tsc
RUN npm run build

FROM nginx:latest

COPY --from=build /usr/local/app/build /usr/share/nginx/html/build
COPY --from=build /usr/local/app/index.html /usr/share/nginx/html
COPY --from=build /usr/local/app/resources /usr/share/nginx/html

EXPOSE 80